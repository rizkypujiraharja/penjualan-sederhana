<?php

use Illuminate\Database\Seeder;
use App\Product;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for($i = 1; $i <= 50; $i++){
            $product = new Product;
            $product->name = 'Baju ' . $faker->city;
            $product->base_cost = rand(30, 35) * 1000;
            $product->price = $product->base_cost + ($product->base_cost * 30 / 100);
            $product->save();
        }
    }
}
