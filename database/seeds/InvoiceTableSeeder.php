<?php

use Illuminate\Database\Seeder;
use App\{Invoice, InvoiceDetail, Product};
use Carbon\Carbon;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $day = $mutable = Carbon::now();
            for ($i=1; $i < 30; $i++) {
                for ($j=1; $j < rand(5, 20); $j++) {
                    $invoice = new Invoice;
                    $invoice->code = time();
                    $invoice->user_id = rand(1, 50);
                    $invoice->total = 0;
                    $invoice->total_earning = 0;
                    $invoice->save();


                    $products = Product::inRandomOrder()->limit(rand(1,3))->get();
                    foreach ($products as $product) {
                        $total = rand(1,5);
                        $invoiceDetail = new InvoiceDetail;
                        $invoiceDetail->invoice_id = $invoice->id;
                        $invoiceDetail->product_id = $product->id;
                        $invoiceDetail->total = $total;
                        $invoiceDetail->base_cost = $product->base_cost;
                        $invoiceDetail->total_base_cost = $product->base_cost * $total;
                        $invoiceDetail->price = $product->price;
                        $invoiceDetail->total_price = $product->price * $total;
                        $invoiceDetail->created_at = $day;
                        $invoiceDetail->updated_at = $day;
                        $invoiceDetail->save();

                        $invoice->total += $invoiceDetail->total_price;
                        $invoice->total_earning += $invoiceDetail->total_price - $invoiceDetail->total_base_cost;
                        $invoice->created_at = $day;
                        $invoice->updated_at = $day;
                    }

                    $invoice->save();
                }

                $day = $day->subDay();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }
}
