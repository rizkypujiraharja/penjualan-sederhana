<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Rizky Puji Raharja';
        $user->email = 'rizky.rsj@gmail.com';
        $user->role = 'admin';
        $user->password = bcrypt('secret');
        $user->save();

        $faker = Faker::create('id_ID');

    	for($i = 1; $i <= 50; $i++){
            $user = new User;
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->role = 'pelanggan';
            $user->password = bcrypt('secret');
            $user->save();

    	}
    }
}
