@extends('layouts.app')

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/apex-chart/apexcharts.css') }}">
@endsection

@section('content')

<div class="col-md-12 col-lg-12">
    <div class="row row-xs clearfix">
        <div class="col-sm-6 col-xl-3">
        <div class="card mg-b-20">
            <div class="card-body pd-y-0">
                <div class="custom-fieldset mb-4">
                    <div class="clearfix">
                    <label>User</label>
                    </div>
                    <div class="d-flex align-items-center text-dark">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-warning">
                        <i class="fa fa-users tx-warning tx-20"></i>
                    </div>
                    <div>
                        <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span class="counter">{{ $count['user'] }}</span></h2>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-xl-3">
        <div class="card mg-b-20">
            <div class="card-body pd-y-0">
                <div class="custom-fieldset mb-4">
                    <div class="clearfix">
                    <label>Product</label>
                    </div>
                    <div class="d-flex align-items-center text-dark">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-success">
                        <i class="fa fa-archive tx-success tx-20"></i>
                    </div>
                    <div>
                        <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span class="counter">{{ $count['product'] }}</span></h2>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-xl-3">
        <div class="card mg-b-20">
            <div class="card-body pd-y-0">
                <div class="custom-fieldset mb-4">
                    <div class="clearfix">
                    <label>Sales</label>
                    </div>
                    <div class="d-flex align-items-center text-dark">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-primary">
                        <i class="icon-handbag tx-primary tx-20"></i>
                    </div>
                    <div>
                        <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span class="counter">{{ $count['sales'] }}</span></h2>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-xl-3">
        <div class="card mg-b-20">
            <div class="card-body pd-y-0">
                <div class="custom-fieldset mb-4">
                    <div class="clearfix">
                    <label>Earnings</label>
                    </div>
                    <div class="d-flex align-items-center text-dark">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-danger">
                        <i class="fa fa-dollar tx-danger tx-20"></i>
                    </div>
                    <div>
                        <h2 class="tx-20 tx-sm-18 tx-md-18 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark"><span class="counter">Rp. {{ $count['earning'] }}</span></h2>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="row row-xs clearfix">
    <div class="col-xl-8 col-md-12 col-lg-12">
        <div class="card mg-b-20">
           <div class="card-header">
              <h4 class="card-header-title">
                 Sales Chart
              </h4>
           </div>
           <div class="card-body collapse show" id="collapse1">
              <div class="clearfix">
                 <div id="basicLineChart"></div>
              </div>
           </div>
        </div>
    </div>
    <div class="col-xl-4 col-md-12 col-lg-12">
        <div class="card mg-b-20">
            <div class="card-header">
                <h4 class="card-header-title">
                    Top Sales
                </h4>
            </div>
            <div class="card-body collapse show" id="collapse1">
                <div class="clearfix">
                    <table class="table table-striped">
                        <thead>
                           <tr>
                              <th>No</th>
                              <th>Product Name</th>
                              <th>Sales</th>
                           </tr>
                        </thead>
                        <tbody>
                            @foreach ($topSales as $topSale)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $topSale->product->name }}</td>
                                <td>{{ $topSale->total }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     </div>

    </div>
</div>
 @endsection

 @section('js')
 <script src="{{ asset('light/assets/plugins/apex-chart/apexcharts.min.js') }}"></script>
 <script>
    $(function(){
        var options = {
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            series: [{
                name: "Desktops",
                data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
            }],
            title: {
                text: '',
                align: 'left'
            },
            grid: {
                row: {
                    colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#basicLineChart"),
            options
        );

	    chart.render();
    });
 </script>
 @endsection
