@extends('layouts.app')

@section('content')

<div class="col-md-12 col-lg-12">
    <div class="card pd-20 wd-80p m-auto">
        <div class="card-header">
            <h4 class="card-header-title">
             Add User
          </h4>
        </div>
        <div class="card-body pd-0">
            <form action="{{ route('user.store') }}" method="post">
            @csrf
            <div class="form-layout form-layout-5">
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Full Name:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <input type="text" class="form-control" placeholder="Enter Full Name" name="name" required>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Email:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <input type="email" class="form-control" placeholder="Enter Email" name="email" required>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Role:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <select name="role" class="form-control">
                            <option value="pelanggan">Pelanggan</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Password:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <input type="email" class="form-control" placeholder="Enter Password" name="password" required>
                    </div>
                </div>
                <div class="row mg-t-30">
                    <div class="col-sm-8 mg-l-auto">
                        <div class="form-layout-footer">
                            <button type="submit" class="btn btn-custom-primary">Create</button>
                            <a href="{{ route('user.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                        <!-- form-layout-footer -->
                    </div>
                    <!-- col-8 -->
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

