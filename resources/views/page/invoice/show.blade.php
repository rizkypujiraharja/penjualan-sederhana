@extends('layouts.app')

@section('content')
<div class="card pd-20 wd-80p m-auto">
    <h5 class="card-title bd-b pd-y-20">Invoice <a href="" class="tx-dark">#{{ $invoice->code }}</a></h5>
    <div class="card-body pd-0 printing-area">
       <div class="row">
          <div class="col-md-3">
             <address>
                <img src="{{ asset('light/assets/images/logo.png') }}" class="img-fluid" alt="logo"><br><br>
                <strong>Kazee Digital Indonesia, Inc.</strong><br>
                Jl. Setrasari indah IV<br>
                Bandung, ID 94103<br>
             </address>
          </div>
          <div class="col-md-5 ml-md-auto text-md-right">
             <h4 class="text-dark">To:</h4>
             <address>
                <strong>{{ $invoice->user->name }}</strong><br>
                <abbr title="Email">E:</abbr> {{ $invoice->user->email }}
             </address>
             <span><strong>Invoice Date:</strong> {{ $invoice->created_at->format('d-M-Y') }}</span>
          </div>
       </div>
       <div class="table-responsive">
          <table class="table table-hover">
             <thead>
                <tr>
                   <th>#</th>
                   <th>Product</th>
                   <th>Quantity</th>
                   <th>Price</th>
                   <th class="text-right">Total</th>
                </tr>
             </thead>
             <tbody>
                 @foreach ($invoice->invoiceDetails as $detail)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $detail->product->name }}</td>
                    <td>{{ $detail->price }}</td>
                    <td>{{ $detail->total }}</td>
                    <td class="text-right">{{ $detail->total_price }}</td>
                 </tr>
                 @endforeach
             </tbody>
          </table>
       </div>
       <br><br>
       <div class="row">
          <div class="col-md-6">
             <h5>Notes</h5>
             <p>Thank you for your orders.</p>
          </div>
          <div class="col-md-4 ml-md-auto text-right">
             <br>
             <p class="tx-bold">Grand Total: <span class="tx-20 tx-gray-900">Rp. {{ $invoice->invoiceDetails()->sum('total_price') }}</span></p>
             <br>
          </div>
       </div>
       <hr>
       <div class="text-right mg-y-20">
          <button type="submit" class="btn btn-custom-primary mg-t-5"><i class="fa fa-dollar"></i> Proceed to payment</button>
          <button type="button" class="btn btn-primary mg-t-5" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</button>
       </div>
    </div>
 </div>
@endsection
