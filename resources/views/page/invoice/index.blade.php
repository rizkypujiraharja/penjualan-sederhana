@extends('layouts.app')

@section('content')

<div class="col-md-12 col-lg-12">
    <div class="card pd-20 wd-80p m-auto">
       <div class="card-header">
          <h4 class="card-header-title">
             invoice
          </h4>
       </div>
       <div class="card-body pd-0 collapse show" id="collapse2">
          <table class="table table-striped">
             <thead>
                <tr>
                   <th>No</th>
                   <th>Kode</th>
                   <th>User</th>
                   <th>Total</th>
                   <th>#</th>
                </tr>
             </thead>
             <tbody>
                @forelse ($invoices as $invoice)
                    <tr>
                        <td>{{ (($invoices->currentPage()-1) * $invoices->perPage()) + $loop->iteration }}</td>
                        <td>{{ $invoice->code }}</td>
                        <td>{{ $invoice->user->name }}</td>
                        <td>RP. {{ number_format($invoice->total, 0,",",".") }}</td>
                        <td>
                            <a href="{{ route('invoice.show', $invoice->id) }}">
                                <button class="btn btn-sm btn-primary">Detail</button>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" align="center">invoice Not Found</td>
                    </tr>
                @endforelse
             </tbody>
          </table>
          {{ $invoices->links() }}
       </div>
    </div>
 </div>
@endsection
