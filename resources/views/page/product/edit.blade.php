@extends('layouts.app')

@section('content')

<div class="col-md-12 col-lg-12">
    <div class="card pd-20 wd-80p m-auto">
        <div class="card-header">
            <h4 class="card-header-title">
             Edit Product
          </h4>
        </div>
        <div class="card-body pd-0">
            <form action="{{ route('product.update', $product) }}" method="post">
                @csrf
                @method('PATCH')
                <div class="form-layout form-layout-5">
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Product Name:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" placeholder="Enter Product Name" value="{{ $product->name }}" name="name" required>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Base Cost:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" placeholder="Enter Base Cost" value="{{ $product->base_cost }}" name="base_cost" required>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Price:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" placeholder="Enter Price" value="{{ $product->price }}" name="price" required>
                        </div>
                    </div>
                    <div class="row mg-t-30">
                        <div class="col-sm-8 mg-l-auto">
                            <div class="form-layout-footer">
                                <button type="submit" class="btn btn-custom-primary">Save</button>
                                <a href="{{ route('product.index') }}" class="btn btn-secondary">Cancel</a>
                            </div>
                            <!-- form-layout-footer -->
                        </div>
                        <!-- col-8 -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

