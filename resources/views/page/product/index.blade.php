@extends('layouts.app')

@section('content')

<div class="col-md-12 col-lg-12">
    <div class="card pd-20 wd-80p m-auto">
       <div class="card-header">
          <h4 class="card-header-title">
             Product
          </h4>
          <div class="float-right">
            <a href="{{ route('product.create') }}" class="btn btn-sm btn-success">Add</a>
          </div>
       </div>
       <div class="card-body pd-0 collapse show" id="collapse2">
          <table class="table table-striped">
             <thead>
                <tr>
                   <th>No</th>
                   <th>Product Name</th>
                   <th>Base Cost</th>
                   <th>Price</th>
                   <th>#</th>
                </tr>
             </thead>
             <tbody>
                @forelse ($products as $product)
                    <tr>
                        <td>{{ (($products->currentPage()-1) * $products->perPage()) + $loop->iteration }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->base_cost }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            <a href="{{ route('product.edit', $product->id) }}">
                                <button class="btn btn-sm btn-warning">Edit</button>
                            </a>
                            <button id="delete" data-title="{{$product->name}}" href="{{ route('product.destroy', $product) }}" class="btn btn-sm btn-danger">Delete</button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" align="center">Product Not Found</td>
                    </tr>
                @endforelse
             </tbody>
          </table>
          {{ $products->links() }}
       </div>
    </div>
 </div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus product bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
