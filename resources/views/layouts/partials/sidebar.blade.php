<div class="page-sidebar">
    <div class="logo">
       <a class="logo-img" href="index.html">
       <img class="desktop-logo" src="{{ asset('light/assets/images/logo.png') }}" alt="">
       <img class="small-logo" src="{{ asset('light/assets/images/small-logo.png') }}" alt="">
       </a>
       <i class="ion-ios-close-empty" id="sidebar-toggle-button-close"></i>
    </div>
    <!--================================-->
    <!-- Sidebar Menu Start -->
    <!--================================-->
    <div class="page-sidebar-inner">
       <div class="page-sidebar-menu">
          <ul class="accordion-menu">
             <li class="{{ (request()->is('dashboard')) ? 'active' : '' }}">
                <a href="{{ route('dashboard.index') }}"><i data-feather="dashboard"></i>
                <span>Dashboard</span></a>
             </li>
             <li class="{{ (request()->is('product*')) ? 'active' : '' }}">
                <a href="{{ route('product.index') }}"><i data-feather="box"></i>
                <span>Product</span></a>
             </li>
             <li class="{{ request()->is('user*') ? 'active' : '' }}">
                <a href="{{ route('user.index') }}"><i data-feather="users"></i>
                <span>User</span></a>
             </li>
             <li class="{{ request()->is('invoice*') ? 'active' : '' }}">
                <a href="{{ route('invoice.index') }}"><i data-feather="users"></i>
                <span>Invoice</span></a>
             </li>
          </ul>
       </div>
    </div>
    <!--/ Sidebar Menu End -->
    <!--================================-->
    <!-- Sidebar Footer Start -->
    <!--================================-->
    <div class="sidebar-footer">
       <a class="pull-left" href="page-profile.html" data-toggle="tooltip" data-placement="top" data-original-title="Profile">
       <i data-feather="user" class="ht-15"></i></a>
       <a class="pull-left " href="mailbox.html" data-toggle="tooltip" data-placement="top" data-original-title="Mailbox">
       <i data-feather="mail" class="ht-15"></i></a>
       <a class="pull-left" href="page-unlock.html" data-toggle="tooltip" data-placement="top" data-original-title="Lockscreen">
       <i data-feather="lock" class="ht-15"></i></a>
       <a class="pull-left" href="page-singin.html" data-toggle="tooltip" data-placement="top" data-original-title="Sing Out">
       <i data-feather="log-out" class="ht-15"></i></a>
    </div>
    <!--/ Sidebar Footer End -->
 </div>
