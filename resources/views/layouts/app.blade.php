<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="keyword" content="">
      <meta name="author"  content=""/>
      <!-- Page Title -->
      <title>Penjualan Sederhana</title>
      <!-- Main CSS -->
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/bootstrap/css/bootstrap.min.css') }}"/>
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/font-awesome/css/font-awesome.min.css') }}"/>
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/flag-icon/flag-icon.min.css') }}"/>
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/simple-line-icons/css/simple-line-icons.css') }}">
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/plugins/ionicons/css/ionicons.css') }}">
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/css/app.min.css') }}"/>
      <link type="text/css" rel="stylesheet" href="{{ asset('light/assets/css/style.min.css') }}"/>
      <!-- Favicon -->
      <link rel="icon" href="{{ asset('light/assets/images/favicon.ico') }}" type="image/x-icon">
      <!-- HTML5 shim and Respond.js' for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js' doesn"t work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js'"></script>
      <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js'"></script>
      <![endif]-->
      @yield('css')
   </head>
   <body>
      <!--================================-->
      <!-- Page Container Start -->
      <!--================================-->
      <div class="page-container">
         <!--================================-->
         <!-- Page Sidebar Start -->
         <!--================================-->
         @include('layouts.partials.sidebar')
         <!--/ Page Sidebar End -->
         <!--================================-->
         <!-- Page Content Start -->
         <!--================================-->
         <div class="page-content">
            <!--================================-->
            <!-- Page Header Start -->
            <!--================================-->
            @include('layouts.partials.header')
            <!--/ Page Header End -->
            <!--================================-->
            <!-- Page Inner Start -->
            <!--================================-->
            <div class="page-inner bg-white">
                <div class="row no-gutters pd-b-20 pd-t-15 wd-100p clearfix">
                    @yield('content')
                </div>
             </div>
            <!--/ Page Inner End -->
            <!--================================-->
            <!-- Page Footer Start -->
            <!--================================-->
            {{-- @include('layouts.partials.footer')     --}}
            <!--/ Page Footer End -->
         </div>
         <!--/ Page Content End  -->
      </div>
      <!--/ Page Container End -->
      <!--================================-->
      <!-- Scroll To Top Start-->

      <!--================================-->
      <a href="#" data-click="scroll-top" class="btn-scroll-top fade"><i class="fa fa-arrow-up"></i></a>
      <!--/ Scroll To Top End -->
      <!--================================-->
      <!-- Footer Script -->
      <!--================================-->
      <script src="{{ asset('light/assets/plugins/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/jquery-ui/jquery-ui.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/popper/popper.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/feather-icon/feather.min.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/pace/pace.min.js') }}"></script>
      <script src="{{ asset('light/assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js') }}"></script>
      <script src="{{ asset('light/assets/js/jquery.slimscroll.min.js') }}"></script>
      <script src="{{ asset('light/assets/js/highlight.min.js') }}"></script>
      <script src="{{ asset('light/assets/js/app.js') }}"></script>
      <script src="{{ asset('light/assets/js/custom.js') }}"></script>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      <script>
        @if(Session::has('alert-error'))
        swal("Gagal!", "{{ Session::get('alert-error')}}", "error");
        @endif
        @if(Session::has('alert-success'))
        swal("Berhasil!", "{{ Session::get('alert-success')}}", "success");
        @endif
      </script>
      @yield('js')
   </body>
</html>
