
# Penjualan Sederhana

#Install
- clone
- composer install
- cp .env.example .env
- php artisan key:generate
- setting up database
- php artisan migrate
- php artisan db:seed
- voila done!

#Keterangan
- Chart Sales Bulanan masih dummy


# Screenshot

## Dashboard
![Dashboar](https://gitlab.com/rizkypujiraharja/penjualan-sederhana/raw/master/dashboard.png)

## List User
![List User](https://gitlab.com/rizkypujiraharja/penjualan-sederhana/raw/master/listUser.png)

## List Product
![List Product](https://gitlab.com/rizkypujiraharja/penjualan-sederhana/raw/master/listproduct.png)

## List Invoice
![List Product](https://gitlab.com/rizkypujiraharja/penjualan-sederhana/raw/master/listInvoice.png)

## Invoice
![List Product](https://gitlab.com/rizkypujiraharja/penjualan-sederhana/raw/master/invoice.png)
