<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User, Product, InvoiceDetail};
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $user = User::get()->count();
        $product = Product::get()->count();
        $sales = InvoiceDetail::sum('total');
        $earning = InvoiceDetail::select(DB::raw('sum(total_price - total_base_cost) as total'))
                                ->first()->total;

        $count['user'] = number_format($user,0,",",".");
        $count['product'] = number_format($product,0,",",".");
        $count['sales'] = number_format($sales,0,",",".");
        $count['earning'] = number_format($earning,0,",",".");

        $topSales = InvoiceDetail::with('product')
                    ->addSelect('product_id', DB::raw('sum(total) as total'))
                    ->groupBy('product_id')
                    ->orderBy('total', 'desc')
                    ->limit(7)
                    ->get();

        return view('page.dashboard.index', compact(['count', 'topSales']));
    }
}
