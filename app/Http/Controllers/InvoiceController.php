<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::with('user')->paginate(10);

        return view('page.invoice.index', compact('invoices'));
    }

    public function show(Invoice $invoice)
    {
        return view('page.invoice.show', compact('invoice'));
    }
}
