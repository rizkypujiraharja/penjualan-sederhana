<?php

use Faker\Factory as Faker;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('out');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::resource('product', 'ProductController');
    Route::resource('user', 'UserController');
    Route::get('invoice', 'InvoiceController@index')->name('invoice.index');
    Route::get('invoice/{invoice}', 'InvoiceController@show')->name('invoice.show');
});
